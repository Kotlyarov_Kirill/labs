#!/bin/bash
echo "Enter process name"
read procName
FILE=/var/log/$procName.log

if [ ! -f $FILE ]
then
echo "Log file $FILE doesn't exist"
exit 1
fi 

echo "Enter first  range hour"
read hh1
echo "Enter first  range minute"
read mm1
echo "Enter second range hour"
read hh2
echo "Enter second range hour"
read mm2
param='['${hh1:0:1}'-'${hh2:0:1}']['${hh1:1:1}'-'${hh2:1:1}']:[0-'${mm1:0:1}'0-'${mm2:0:1}'][0-'${mm1:1:1}'0-'${mm2:1:1}']:[0-9][0-9]'
echo $param

grep $param /var/log/$procName.log               
