#!/bin/bash
for i in `seq 0 $2` 
do
cd $1
mkdir "dir$i"
cd ./dir$i
let "n = $1 * 5"
for i in `seq 0 $n` 
do
mkdir "subdir$i"
cd ./subdir$i
let "m = $2 * 4"
for i in `seq 0 $m` 
do
touch file$i.file
done
cd ..
done
cd ..
done
